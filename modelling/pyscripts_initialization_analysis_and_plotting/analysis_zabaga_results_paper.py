#!/usr/bin/env python

# This python script plots organism derived results.

###########################################################

# General script parameters and strings

num_traj_phasespace=20;
plot_traj_in_phasespace=1  # set this parameter to 1 to do this plot

# Plotting related parameters 
fz=12  # font size for the different plots
lw=2.0 # linewidth of the plots
lwthin=1.5 # linewidth of the plots

fiz=4       # Figure size
plot_ic=1   # Set it to 1 to plot initial conditions, set it to 1 in the corresponding plots
lwth=4.0;   # Line width

## Color definitions ##
alphth=0.6;
colth='r';

# representation range plot
xmin=0
ymin=0
ymax=200
xmax=1000

nbins=40;

variables_tc=['aba','ga','target','modulator'] # variables to represent in the timecourses
variables_histos=['aba','ga','target','germinationtime'] # variables to represent in the histograms
variables_histos=['germinationtime'] # variables to represent in the histograms

labels={'aba':'$[ABA]$','ga':'$[GA]$','target':'$[RGL2]$','germinationtime':'$T_g$ (A.U.)','modulator':'GA dependent production',}

labels={'aba':'$[ABA]$','ga':'$[GA]$','target':'$[I]$','germinationtime':'$T_g$ (A.U.)','modulator':'$[Z]$'}

# Importing packages###############
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
matplotlib.rcParams['pdf.fonttype'] = 42

import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio

import eqs
from eqs import *

#############################################

matplotlib.pyplot.ioff()


dirini=os.getcwd()

with open("info.json", "r") as f:
    info= json.load(f)

#info= pickle.load(open('info.pkl'))    #reading parameters

ga_ic=info['initial_conds_pars']['ga_ic']; # 
aba_ic=info['initial_conds_pars']['aba_ic']; #
target_ic=info['initial_conds_pars']['target_ic']; # 
thresholds_for_germination=info['initial_conds_pars']['thresholds_for_germination']; # 
num_seeds=info['initial_conds_pars']['num_seeds']

num_seeds_to_plot=min(200,num_seeds);      # number of seeds trajectories that will be plotted in the timecourses, etc

deg_aba=info['model_pars']['deg_aba']; # 
deg_ga=info['model_pars']['deg_ga']; # 
basal_aba=info['model_pars']['basal_aba']; # 
basal_ga=info['model_pars']['basal_ga']; # 
bastime_ga=info['model_pars']['bastime_ga'];

basal_m=info['model_pars']['basal_m'];
deg_m=info['model_pars']['deg_m'];

vmax_aba=info['model_pars']['vmax_aba']; # 
vmax_ga=info['model_pars']['vmax_ga']; # 
seed_vol=info['model_pars']['seed_vol']; # 

theta_aba=info['model_pars']['theta_aba']; # 
theta_ga=info['model_pars']['theta_ga'] # 
hhh=info['model_pars']['hhh'] #

basal_target=info['model_pars']['basal_target']; # 
deg_target=info['model_pars']['deg_target']; # 
vmax_z=info['model_pars']['vmax_z']; #
theta_zaba=info['model_pars']['theta_zaba']; #
delta_1aba=info['model_pars']['delta_1aba']; #
h_1=info['model_pars']['h_1'] #
deg2_target=info['model_pars']['deg2_target']; #
theta_zga=info['model_pars']['theta_zga']; #
delta_2ga=info['model_pars']['delta_2ga']; #
h_2=info['model_pars']['h_2'] #

added_ga=info['doses_pars']['added_ga']
added_aba=info['doses_pars']['added_aba']

plotting_individual_simulations_figs=info['plotting_pars']['plotting_individual_simulations_figs']
plot_histos=info['plotting_pars']['plot_histos']
aspect_histos=info['plotting_pars']['aspect_histos']
plot_timecourses=info['plotting_pars']['plot_timecourses']
plot_phasediag=info['plotting_pars']['plot_phasediag']
plot_target_vs_gaaba=info['plotting_pars']['plot_target_vs_gaaba']
plot_timecourse_germination_fraction=info['plotting_pars']['plot_timecourse_germination_fraction']
plot_timecourses_productionga=info['plotting_pars']['plot_timecourses_productionga']

plot_theolines=info['plotting_pars']['plot_theolines']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']
get_obs=info['plotting_pars']['get_obs']

abaga_path=info['strings']['main_path']

plot_separated_nuclines=1

if 'main_doses_path' in info['strings'] :
    main_sims_path=info['strings']['main_doses_path']
elif 'main_parsexploration_path' in info['strings'] :
    main_sims_path=info['strings']['main_parsexploration_path']

plot_fc=0
plot_icl=0
plot_funs=0
plot_iuns=0
plot_fc_max=0

string_single_plot='_added_ga'+str(added_ga)+'_added_aba'+str(added_aba)

if 'varying_par' in info :
    dimsweep=info['varying_par']['dimsweep']
    if dimsweep==2 :
        string_single_plot='_theta_zaba'+str(theta_zaba)+'_theta_zga'+str(theta_zga)+'_seed_vol'+str(seed_vol)+'_basal_aba'+str(basal_aba)+'_deg_aba'+str(deg_aba)

        if get_obs==1 :
            numroots=info['analysis']['start_num_fp']
            fnumroots=info['analysis']['final_num_fp']
            if 'target_fc' in info['analysis'] :
                plot_fc=1
                target_fc=info['analysis']['target_fc']
            if 'target_fc_max' in info['analysis'] :
                plot_fc_max=1
                target_fc_max=info['analysis']['target_fc_max']
            if 'target_icl' in info['analysis'] :   
                plot_icl=1   
                target_icl=info['analysis']['target_icl']
            if 'target_funs' in info['analysis'] :
                plot_funs=1
                target_funs=info['analysis']['target_funs']
            if 'target_iuns' in info['analysis'] :
                plot_iuns=1
                target_iuns=info['analysis']['target_iuns']


#            newinfo={'analysis':{'start_num_fp' : numroots,'final_num_fp' : fnumroots,'target_fc': zf}}

#critical_gaaba_fraction=thresholds_for_germination*deg_target/vmax_target

filedat='organism.gdata' # input file name #organism.gdata contains seed timecourses with variables; located in Results folder #names are columns in organism.gdata file

if os.path.isfile(filedat) :
    x = pd.read_csv(filedat,sep=' ',skip_blank_lines=True,header=0,index_col=False,names=['step','time','seed_id','YY','XY','XX','volume','aba','ga','target','threshold','germinationtime','NoiseOneaba','NoiseTwoga','modulator','q1','q2','q3','q4','q5','q6','q7','q8','q9','q10','q11'])

#print(x['time'].values)
time_points=x['time'].unique()
step_points=x['step'].unique()

fin_time= max(x.time);
init_time= min(x.time);


ids_germinated_seeds=x[(x.time==fin_time) & (x.germinationtime>0)].seed_id.unique()
num_germinated_seeds=len(ids_germinated_seeds)

times_histos=[init_time,fin_time]

seeds_indices=x['seed_id'].unique()

# Computing germination fractions
germination_fractions=np.array([])
for jj in range(0,len(time_points),1) :
        times=time_points[jj]
        germinated_seeds_ids=x[(x.time==times) & (x.germinationtime>0)]['seed_id']
        number_germinated_seeds=len(germinated_seeds_ids)
        fraction_germinated_seeds=1.0*number_germinated_seeds/num_seeds
        germination_fractions=np.append(germination_fractions,fraction_germinated_seeds)

sample=x[(x.time==fin_time) & (x.germinationtime>0)]['germinationtime'].values
#sample_truncated=x[(x.time==30) & (x.germinationtime>0)]['germinationtime'].values

sz=size(sample)

if len(sample)>9 :
    mn_gt=np.mean(sample)
    std_gt=np.std(sample)
    mode_array=stats.mode(sample) #get modal values as an array
    if (len(mode_array)>1) :
        mode_gt=mode_array[0][0]  #extract modal values from array
    else :
        mode_gt=np.NAN

    min_gt=np.amin(sample)
    max_gt=np.amax(sample)
    range_gt=max_gt-min_gt

    cv_gt=(1.0*std_gt/mn_gt)
    q1_gt=np.percentile(sample,25)
    q2_gt=np.percentile(sample,50)
    q3_gt=np.percentile(sample,75)
    cvq_gt=1.0*(q3_gt-q1_gt)/q2_gt
else :
    #sz=0
    mn_gt=np.NAN
    std_gt=np.NAN
    mode_gt=np.NAN #get modal values as an array

    min_gt=np.NAN
    max_gt=np.NAN
    range_gt=np.NAN

    cv_gt=np.NAN
    q1_gt=np.NAN
    q2_gt=np.NAN
    q3_gt=np.NAN
    cvq_gt=np.NAN


if 'varying_par' in info :
    par1=info['varying_par']['par1']
    par2=info['varying_par']['par2']

    if dimsweep==1 :
        obs_final=[(info['model_pars'][par1],fraction_germinated_seeds,cv_gt,mn_gt,std_gt,mode_gt,min_gt,max_gt,range_gt,q1_gt,q2_gt,q3_gt,cvq_gt,sz)]
        x_obs=pd.DataFrame(obs_final, columns=[par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size'])
    elif dimsweep==2 :
        if get_obs==1 :
            # if target initial condition above threshold, set it to 1.
            ithtar=0
            if target_ic>thresholds_for_germination :
            #if target_ic>0.01 :
                ithtar=1
            # if target final condition is below threshold, set it to 1 
            fthtar=0
            if target_fc<thresholds_for_germination :
                fthtar=1
            
            obs_final=[(info['model_pars'][par1],info['model_pars'][par2],fraction_germinated_seeds,cv_gt,mn_gt,std_gt,mode_gt,min_gt,max_gt,range_gt,q1_gt,q2_gt,q3_gt,cvq_gt,sz,ithtar,fthtar,numroots,fnumroots)]
            x_obs=pd.DataFrame(obs_final, columns=[par1,par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp'])

    if get_obs== 1 : 
        x_obs.to_csv(path_or_buf='obs.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

else:
    # Generating panda with main observables, and exporting it into a data file
    obs_final=[(added_aba,added_ga,fraction_germinated_seeds,cv_gt,mn_gt,std_gt,mode_gt,min_gt,max_gt,range_gt,q1_gt,q2_gt,q3_gt,cvq_gt,sz)]
    x_obs=pd.DataFrame(obs_final, columns=['added_aba','added_ga','fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size'])
    x_obs.to_csv(path_or_buf='obs.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

if num_germinated_seeds==0 :
    germinated_target='NaN'
    mn_target='NaN'
    var_target='NaN'
    std_target='NaN'

# Computing mean and variance of target distribution of germinated seeds
for ii in range(0,number_germinated_seeds,1) :
        germinated_target=x[(x.time==fin_time) & (x.germinationtime>0)]['target'].values
        mn_target=np.mean(germinated_target)    # mean of target distribution
        var_target=np.var(germinated_target)    # variance of target distribution
        std_target=np.std(germinated_target)

# Generating panda with stats for target
obs_target=[(mn_target,var_target,std_target)]
x_obstarg=pd.DataFrame(obs_target,columns=['mn_target','var_target','std_target'])
x_obstarg.to_csv(path_or_buf='obstarg.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

if plotting_individual_simulations_figs==1 :
    # Plotting figures
    fig_size=(0.7*fiz,0.7*fiz)   #for starting the plots
    params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
    rcParams['font.family'] = 'Arial'
    rcParams['pdf.fonttype'] = 42
    pylab.rcParams.update(params)

    #conditionals
    if plot_phasediag==1 :
        dirout_nullclines_summary=abaga_path+'/'+main_sims_path+'/nullcline_plots/'
        try:
            os.stat(dirout_nullclines_summary)
        except:
            os.mkdir(dirout_nullclines_summary)

        #print('plotting phase diagrams')
        fig_size=2
        numtheopoints=400;  # Number of theoretical points used along the theoretical line.
        
        list_X=[]
        
        list_X2=np.logspace(-3, 2, num=numtheopoints)
        list_X=np.logspace(-1.5, 1.5, num=numtheopoints)

        nullcl1=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(list_X,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)
        nullcl2=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(list_X,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)

        nullcl_ga1=gfunct(list_X2,basal_ga,vmax_ga,deg_ga,theta_ga,hhh)
        nullcl_ga2=gfunct(list_X2,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)

        pert_nullcl1=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)+added_aba,gfunct(list_X,basal_ga,vmax_ga,deg_ga,theta_ga,hhh)+added_ga,basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)
        pert_nullcl2=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)+added_aba,gfunct(list_X,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)+added_ga,basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)

        threshold_in_phasediagram=np.zeros(len(list_X))+thresholds_for_germination

        xmax=np.max(list_X)
        ymin=np.min(list_X)
        ymax=np.max(list_X)
        #ymax=100
        #xmax=100

        fig=plt.figure()
        ax = fig.add_subplot(111) 
        plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)
                                               
        plt.xlabel(r"[$GA$]",verticalalignment='top',fontsize=fz)
        plt.ylabel(r"[$I$]",fontsize=fz)

        plt.loglog(nullcl_ga1,list_X2,'k-',linewidth=lw,color = '0.75')
        plt.loglog(nullcl_ga2,list_X2,'k--',linewidth=lw,color = '0.75')

        if (plot_traj_in_phasespace==1) :
            for jj in range(0,num_traj_phasespace,1) :        # for loop to plot a certain number of seeds
                ind=seeds_indices[jj]
                xs=x[(x.seed_id==ind)]['ga'].values[::1]
                ys=x[(x.seed_id==ind)]['target'].values[::1]
                plt.loglog(xs,ys,'-',linewidth=lwthin);

        if (plot_ic==1) :
            plt.loglog(ga_ic,target_ic,'wo',markersize=8,markeredgecolor='k',markeredgewidth=2.0)

        plt.loglog(list_X2,threshold_in_phasediagram,'-.',color='b',linewidth=lw);

        #plt.xticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
        #plt.yticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  

        list_XX=np.logspace(-3, 2, num=6)

        plt.xticks(list_XX,fontsize=fz)
        plt.yticks(list_XX,fontsize=fz)

        #plt.axis('equal')
        #plt.axis([xmin, xmax,ymin, ymax])
        
        #plt.tight_layout()

        fname="phasespace_and_traj_seeds.pdf"
        # saving the figure
        plt.savefig(fname)

        # close the figure
        plt.close()

        fig=plt.figure()
        ax = fig.add_subplot(111) 
        plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)                                            

        plt.xlabel(r"[$I$]",verticalalignment='top',fontsize=fz)
        plt.ylabel(r"[$I$]",fontsize=fz)

        plt.loglog(list_X,list_X,'k-',linewidth=lw,color = '0.35')
        plt.loglog(list_X,nullcl1,'k-',linewidth=lw,color = '0.75')
        plt.loglog(list_X,nullcl2,'k--',linewidth=lw,color = '0.75')

        if added_ga>0 :
            plt.loglog(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)
            plt.loglog(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

        if added_aba>0 :
            plt.loglog(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)
            plt.loglog(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

        if (plot_ic==1) :
            #plt.loglog(target_ic,target_ic,'wo',markersize=6,markeredgecolor='k',markeredgewidth=2.0)
            #plt.loglog(target_ic,target_ic,'o',markersize=6,markeredgecolor='k',color='0.65',markeredgewidth=1.0)
            plt.loglog(target_ic,target_ic,'ko',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

            if (plot_icl==1) :
                plt.loglog(target_icl,target_icl,'ko',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

            if plot_funs==1 :
                plt.loglog(target_funs,target_funs,'wd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)
            if plot_iuns==1 :
                plt.loglog(target_iuns,target_iuns,'wo',markersize=6,markeredgecolor='k',markeredgewidth=1.0)


        if plot_fc_max==1 :
            #plt.loglog(target_fc_max,target_fc_max,'wd',markersize=6,markeredgecolor='k',markeredgewidth=2.0)
            #plt.loglog(target_fc_max,target_fc_max,'d',markersize=6,markeredgecolor='k',color='0.65',markeredgewidth=1.0)
            plt.loglog(target_fc_max,target_fc_max,'kd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)
        if plot_fc==1 :
            plt.loglog(target_fc,target_fc,'kd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

        plt.loglog(list_X,threshold_in_phasediagram,'-.',color='b',linewidth=lw);

        #plt.xticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
        #plt.yticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
        list_XX=np.logspace(-1, 2, num=4)

        #plt.xticks(list_XX,fontsize=fz)
        #plt.yticks(list_XX,fontsize=fz)

        #plt.axis('equal')
        #plt.axis([xmin, xmax,ymin, ymax])

        #plt.tight_layout()

        fname="phasespace_log.pdf"
        fname='phasespace_log_'+string_single_plot+'.pdf'
        # saving the figure
        plt.savefig(fname)

        shutil.copyfile(fname,dirout_nullclines_summary+fname)

        # close the figure
        plt.close()

        if plot_separated_nuclines==1 :
            
            fig=plt.figure()
            ax = fig.add_subplot(111) 
            plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)                                            

            plt.xlabel(r"[$I$]",verticalalignment='top',fontsize=fz)
            plt.ylabel(r"[$I$]",fontsize=fz)

            plt.loglog(list_X,list_X,'k-',linewidth=lw,color = '0.35')
            plt.loglog(list_X,nullcl1,'k-',linewidth=lw,color = '0.75')

            if added_ga>0 :
                plt.loglog(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)

            if added_aba>0 :
                plt.loglog(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)

            if (plot_ic==1) :
                plt.loglog(target_ic,target_ic,'ko',markersize=6,markeredgecolor='k',markeredgewidth=1.0)
#                plt.loglog(target_ic,target_ic,'o',markersize=6,markeredgecolor='k',color='0.65',markeredgewidth=1.0)

                if (plot_icl==1) :
                    plt.loglog(target_icl,target_icl,'ko',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

            if plot_iuns==1 :
                plt.loglog(target_iuns,target_iuns,'wo',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

            plt.loglog(list_X,threshold_in_phasediagram,'-.',color='b',linewidth=lw);

            #plt.xticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
            #plt.yticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  

            #plt.xticks(list_XX,fontsize=fz)
            #plt.yticks(list_XX,fontsize=fz)

            #plt.tight_layout()

            fname='phasespace_log_'+string_single_plot+'_beforeGAincrease.pdf'
            plt.savefig(fname)
            shutil.copyfile(fname,dirout_nullclines_summary+fname)

            # close the figure
            plt.close()

            fig=plt.figure()
            ax = fig.add_subplot(111) 
            plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)                                            

            plt.xlabel(r"[$I$]",verticalalignment='top',fontsize=fz)
            plt.ylabel(r"[$I$]",fontsize=fz)

            plt.loglog(list_X,list_X,'k-',linewidth=lw,color = '0.35')
            plt.loglog(list_X,nullcl2,'k--',linewidth=lw,color = '0.75')

            if added_ga>0 :
                plt.loglog(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

            if added_aba>0 :
                plt.loglog(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

            if plot_fc_max==1 :
                #plt.loglog(target_fc_max,target_fc_max,'d',markersize=6,markeredgecolor='k',color='0.75',markeredgewidth=1.0)
                plt.loglog(target_fc_max,target_fc_max,'kd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)
            if plot_fc==1 :
                plt.loglog(target_fc,target_fc,'kd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)
            
            if plot_funs==1 :
                plt.loglog(target_funs,target_funs,'wd',markersize=6,markeredgecolor='k',markeredgewidth=1.0)

            plt.loglog(list_X,threshold_in_phasediagram,'-.',color='b',linewidth=lw);

            #plt.xticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
            #plt.yticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  

            #plt.xticks(list_XX,fontsize=fz)
            #plt.yticks(list_XX,fontsize=fz)

            #plt.tight_layout()

            fname='phasespace_log_'+string_single_plot+'_afterGAincrease.pdf'
            plt.savefig(fname)
            shutil.copyfile(fname,dirout_nullclines_summary+fname)

            # close the figure
            plt.close()



        fig=plt.figure()
        ax = fig.add_subplot(111) 
        plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)                                            
        
        linmax=100
        list_X=np.linspace(0, linmax, num=numtheopoints)

        plt.xlabel(r"[$I$]",verticalalignment='top',fontsize=fz)
        plt.ylabel(r"[$I$]",fontsize=fz)

        nullcl1=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(list_X,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)
        nullcl2=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(list_X,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)

        pert_nullcl1=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)+added_aba,gfunct(list_X,basal_ga,vmax_ga,deg_ga,theta_ga,hhh)+added_ga,basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)
        pert_nullcl2=zfunc1(afunct(list_X,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)+added_aba,gfunct(list_X,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)+added_ga,basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)

        plt.plot(list_X,list_X,'k-',linewidth=lw,color = '0.35')
        plt.plot(list_X,nullcl1,'k-',linewidth=lw,color = '0.75')
        plt.plot(list_X,nullcl2,'k--',linewidth=lw,color = '0.75')

        if added_ga>0 :
            plt.plot(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)
            plt.plot(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

        if added_aba>0 :
            plt.plot(list_X,pert_nullcl1,'r-',linewidth=lw,alpha=0.5)
            plt.plot(list_X,pert_nullcl2,'r--',linewidth=lw,alpha=0.5)

        if (plot_ic==1) :
            plt.plot(target_ic,target_ic,'wo',markersize=8,markeredgecolor='k',markeredgewidth=2.0)

        plt.plot(list_X,threshold_in_phasediagram,'-.',color='b',linewidth=lw);


        #plt.xticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
        #plt.yticks(np.arange(-1,6,1), (r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"),fontsize=fz)  
        list_XX=np.linspace(0, linmax, num=4)

        plt.xticks(list_XX,fontsize=fz)
        plt.yticks(list_XX,fontsize=fz)

        #plt.axis('equal')
        plt.axis([0, linmax,0, linmax])

        #plt.tight_layout()

        fname="phasespace.pdf"
        # saving the figure
        plt.savefig(fname)
        plt.close()


    if plot_timecourses==1 :
        #print('plotting timecourses')

        dirout = 'timecourses'  #creating sub-directory
        
        try:
            os.stat(dirout)
        except:
            os.mkdir(dirout)

        dirout_tc_summary=abaga_path+'/'+main_sims_path+'/summary_single_simulations_plots_tc/'
        try:
            os.stat(dirout_tc_summary)
        except:
            os.mkdir(dirout_tc_summary)


        fig_size=16

        for ii in range(0,len(variables_tc),1) :                   #for loop for specific variables
            fig, (ax1) =plt.subplots(1,1)                           # figures for variables
            plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

            var=variables_tc[ii]    #get name of variable 
            xmin=0;xmax=max(x.time)*1.01;
            xmax=end_plotting_time;
          #  xmax=500;

            ymin=min(0,min(x[var])*1.05);ymax=max(x[var])*1.05;

            scale='linear'

            for jj in range(0,num_seeds_to_plot,1) :        #for loop to get certain seeds to plot
                ind=seeds_indices[jj]                   #seed_indices defined at start
                xs=x[(x.seed_id==ind)].time.values      #x values of time course for one seed
                ys=x[(x.seed_id==ind)][var].values      #ind is an index
                plt.plot(xs,ys,'-',linewidth=lw);
            if var=='target' :
                plt.axhline(y=thresholds_for_germination,linewidth=lwth, color='b',linestyle='-.',alpha=alphth)

                if plot_theolines==1 :
                    def C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba) :
                        return (basal_target+ vmax_z*(aba + added_aba)**h_1/(theta_zaba**h_1 + (aba + added_aba)**h_1))

                    def D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga) :     
                        return deg_target + deg2_target*(ga+added_ga)**h_2/(theta_zga**h_2 + (ga + added_ga)**h_2)

                    def tgp(t,seed_vol,z0,v0,basal_target, vmax_z,added_aba,h_1,theta_zaba,deg_target, deg2_target, added_ga,h_2,theta_zga,ga,aba) :
                        cc=C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)
                        dd=D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)
                        radicand=(e**(-2*dd*t)*dd*(v0*seed_vol - z0) + e**(-dd*t)*(dd*z0-cc)+cc)/(dd*seed_vol)
                        #if radicand<0 :
                        #    radicand=0
                        return (e**(-dd*t)*(cc*(-1+ e**(dd*t)) + dd*z0))/dd+ sqrt(radicand)
                        #return (e**(-D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)*(-1 + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)) + D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*z0))/D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)+ sqrt((e**(-2*D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*(v0 - z0) + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)*(-1 + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)) + D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*z0*seed_vol)))/(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*seed_vol))
                        #return (e**(-D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)*(-1 + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)) + D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*z0))/D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)+ sqrt((e**(-2*D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*(v0 - z0) + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)*(C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)*(-1 + e**(D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*t)) + D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)*z0)))/D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga))



                    def tgn(t,seed_vol,z0,v0,basal_target, vmax_z,added_aba,h_1,theta_zaba,deg_target, deg2_target, added_ga,h_2,theta_zga,ga,aba) :
                        dd=D(deg_target, deg2_target, added_ga,h_2,theta_zga,ga)
                        cc=C(basal_target, vmax_z,added_aba,h_1,theta_zaba,aba)
                        radicand=(e**(-2*dd*t)*dd*(v0*seed_vol - z0) + e**(-dd*t)*(dd*z0-cc)+cc)/(dd*seed_vol)
                        #if radicand<0 :
                        #    radicand=0
                        return (e**(-dd*t)*(cc*(-1+ e**(dd*t)) + dd*z0))/dd- sqrt(radicand)



                    ga = ga_ic;
                    aba = aba_ic;
                    aga=6
                    aba=1
                    v0=20;

                    times=x.time.unique();
                    tgp_points=tgp(times,seed_vol,target_ic,v0,basal_target, vmax_z,added_aba,h_1,theta_zaba,deg_target, deg2_target, added_ga,h_2,theta_zga,ga,aba)
                    tgn_points=tgn(times,seed_vol,target_ic,v0,basal_target, vmax_z,added_aba,h_1,theta_zaba,deg_target, deg2_target, added_ga,h_2,theta_zga,ga,aba)

                    plt.plot(times,tgp_points,'k--',linewidth=lw);
                    plt.plot(times,tgn_points,'k--',linewidth=lw);


            plt.xlabel('Time (A.U.)',fontsize=fz)
            plt.ylabel(labels[var],fontsize=fz)
            plt.yscale(scale, nonposy='clip');
            plt.axis([xmin, xmax,ymin, ymax])
            #plt.xticks(np.arange(0,xmax,step=50),fontsize=fz)
            xlim(right=end_plotting_time)
            plt.xticks(np.arange(0,xmax,step=ticks_separation),fontsize=fz)

            plt.yticks(fontsize=fz)
            plt.tight_layout()
            
            fileout='_'+var+'_'+str(num_seeds_to_plot)
            if var=='germinationtime' :
                fileout='_'+var+'_'+str(num_seeds_to_plot)+string_single_plot
            fname="tc"+fileout+".pdf"
            fullfname=dirout+'/'+fname
            plt.savefig(fullfname, format=None)
            if var=='germinationtime' :
                shutil.copyfile(fullfname,dirout_tc_summary+fname)
            plt.close()



    if plot_timecourses_productionga==1 :
        dirout = 'timecourses'  #creating sub-directory

        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        var='modulator'    #get name of variable 
        xmin=0;xmax=max(x.time)*1.01;
        xmax=end_plotting_time;
      #  xmax=500;

        ymin=min(0,min(x[var])*1.05);ymax=(max(x[var])*1.0*bastime_ga+basal_ga)*1.05;

        scale='linear'

        for jj in range(0,num_seeds_to_plot,1) :        #for loop to get certain seeds to plot
            ind=seeds_indices[jj]                   #seed_indices defined at start
            xs=x[(x.seed_id==ind)].time.values      #x values of time course for one seed
            ys=x[(x.seed_id==ind)][var].values*1.0*bastime_ga+basal_ga      #ind is an index
            plt.plot(xs,ys,'-',linewidth=lw);

        plt.xlabel('Time (A.U.)',fontsize=fz)
        plt.ylabel('GA basal production',fontsize=fz)
        plt.yscale(scale, nonposy='clip');
        plt.axis([xmin, xmax,ymin, ymax])
        #plt.xticks(np.arange(0,xmax,step=50),fontsize=fz)
        plt.xticks(np.arange(0,xmax,step=ticks_separation),fontsize=fz)

        plt.yticks(fontsize=fz)
        plt.tight_layout()

        fileout='_'+'basalga'+'_'+str(num_seeds_to_plot)
        fname="tc"+fileout+".pdf"
        fullfname=dirout+'/'+fname
        plt.savefig(fullfname, format=None)
        plt.close()


    if plot_histos==1 :
        #print('plotting histograms')

        dirout = 'histos'
        try:
            os.stat(dirout)
        except:
            os.mkdir(dirout)


        dirout_histos_summary=abaga_path+'/'+main_sims_path+'/summary_single_simulations_plots/'
        try:
            os.stat(dirout_histos_summary)
        except:
            os.mkdir(dirout_histos_summary)

        for ii in range(0,len(variables_histos),1) :
            varname=variables_histos[ii];
            xmin=0;

            xmax=max(x[varname].values)*1.05;
            #if varname=='germinationtime' :
            #xmax=fin_time

            #xmax=100;

            ymin=0;
            ymax=num_seeds;


            for jj in range(0,len(times_histos),1) :
                time=times_histos[jj]
                #pylab.figure()

                fig, (ax) =plt.subplots(1,1)                           # figures for variables
                plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

                sample=x[x.time==time][varname].values

                if (varname=='germinationtime') :
                    sample=x[(x.time==time) & (x.germinationtime>0.0)][varname].values
                    #print(sample)
                #plt.xlabel(labels[varname])
                #plt.ylabel(' $\%$ total seeds ')
                #plt.ylabel('Percentage')

                #if (varname=='germinationtime') :
                    #plt.xlabel(labels[varname],fontsize=fz)
                    #plt.ylabel(' $\%$ germinated seeds ',fontsize=fz)
                    #plt.ylabel('Percentage',fontsize=fz)
                    
                plt.axis([xmin, xmax,ymin, ymax])
                #plt.xticks(np.arange(-1,-0.1,0), (r"1",r"1",r"1",r"1",r"1"))

                yymax=ymax
                if varname=='germinationtime' :
                    if len(sample)>1 :
                        mn=np.mean(sample)
                        std=np.std(sample)
                        cv=std/mn

                        yymax=num_germinated_seeds

                        #plt.text(xmax*0.38, ymax*0.66, r'Mean='+str(format(int(round(mn)),'d')), fontsize=fz-2)
                        #plt.text(xmax*0.40, ymax*0.80, str(format(int(round(fraction_germinated_seeds*100.0)),'d'))+ r' $\%$ germination', fontsize=fz-2)
                        #plt.text(xmax*0.40, ymax*0.60, r'CV='+str(format(cv_gt, '.3f')), fontsize=fz-2)
                        #plt.text(xmax*0.40, ymax*0.40, r'Mode='+str(format(int(round(mode_gt)),'d')), fontsize=fz-2)
                        features=r'CV='+str(format(cv_gt, '.2f'))+'\n'+r'mode='+str(format(int(round(mode_gt)),'d'))+'\n'+r'$\%=$'+str(format(int(round(fraction_germinated_seeds*100.0)),'d'))
                        plt.text(end_plotting_time*0.60, yymax*0.48,features, fontsize=fz-2)
             
                        #print (str(format(cv_gt, '.3f')))
                        #print (str(format(cv, '.3f')))
                        #print str(cv_gt)
                        #print str(cv)
                        #plt.yticks(np.arange(0,ymax*1.01,ymax/(5*4.0)), (r"0",r"5",r"10",r"15"))
                        #plt.text(xmax*0.38, yymax/4*0.75, r'CV='+str(format(cv, '.3f')), fontsize=fz-2)

                        #yymax=num_seeds
                        xmax=end_plotting_time

                        

           
                if varname=='target' :
                    xxmax=max(x['target'])*1.05;
                    xmax=xxmax

                if len(sample)>0 :
                    hist(sample,bins=nbins,range=(0,xmax),facecolor='green', alpha=0.75)

                plt.yticks(np.arange(0,yymax*1.01,yymax/4.0), (r"0",r"25",r"50",r"75",r"100"))
                plt.axis([xmin,xmax,ymin,yymax])

                fileout='_'+varname+'_t'+str(time)

                if varname=='germinationtime' :
                    plt.xticks(np.arange(0,xmax,step=ticks_separation),fontsize=fz)
                    plt.yticks(fontsize=fz)
                    #plt.yticks(np.arange(0,yymax*1.01,yymax/5.0), (r"0",r"20",r"40",r"60",r"80",r"100"))
                    plt.yticks(np.arange(0,yymax*1.01,yymax/4.0), (r"0",r"25",r"50",r"75",r"100"))

                    plt.axis([xmin,xmax,ymin,yymax])
                    
                    fileout='_'+varname+'_t'+str(time)+string_single_plot

                    # print('ratio '+str(rat))
                    if (xmax-xmin) > 0 :
                    # if np.isfinite(xmax-xmin) :
                        rat = 1.0*(yymax-ymin)/(1.0*(xmax-xmin))
                        #plt.axes().set_aspect(0.35/rat)
                        ax.set_aspect(aspect_histos/rat)
                else :
                    plt.tight_layout()

                fname="histo"+fileout+".pdf"
                fullfname=dirout+'/'+fname
                plt.savefig(fullfname, format=None)
                plt.close()
                if varname=='germinationtime' and time>0 :
                    shutil.copyfile(fullfname,dirout_histos_summary+fname)
                    #shutil.copyfile(fullfname,dirout_histos_summary)
                    #shutil.copyfile(fullfname,'../'+diroutdirout_histos_summary)
        
        # plotting histograms of germinationtime until timepoint 100
        xend_plotting_time=100
        xticks_separation=20

        varname='germinationtime';
        xmin=0;

        xmax=max(x[varname].values)*1.05;
        #if varname=='germinationtime' :
        #xmax=fin_time

        #xmax=100;

        ymin=0;
        ymax=num_seeds;


        for jj in range(0,len(times_histos),1) :
            time=times_histos[jj]
            if time>0 :
                fig, (ax) =plt.subplots(1,1)                           # figures for variables
                plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

                sample=x[x.time==time][varname].values

                if (varname=='germinationtime') :
                    sample=x[(x.time==time) & (x.germinationtime>0.0)][varname].values

                plt.axis([xmin, xmax,ymin, ymax])
                yymax=ymax
                if varname=='germinationtime' :
                    if len(sample)>1 :
                        mn=np.mean(sample)
                        std=np.std(sample)
                        cv=std/mn
                        #plt.text(xmax*0.38, ymax*0.66, r'Mean='+str(format(int(round(mn)),'d')), fontsize=fz-2)
                        #plt.text(xmax*0.38, ymax*0.58, r'CV='+str(format(cv, '.3f')), fontsize=fz-2)
                        #plt.yticks(np.arange(0,ymax*1.01,ymax/(5*4.0)), (r"0",r"5",r"10",r"15"))
                        yymax=num_germinated_seeds
                        #plt.text(xmax*0.38, yymax/4*0.75, r'CV='+str(format(cv, '.3f')), fontsize=fz-2)
                        features=r'CV='+str(format(cv_gt, '.2f'))+'\n'+r'mode='+str(format(int(round(mode_gt)),'d'))+'\n'+r'$\%=$'+str(format(int(round(fraction_germinated_seeds*100.0)),'d'))
                        plt.text(100*0.60, yymax*0.48,features, fontsize=fz-2)
                        
                        #yymax=num_seeds
                        xmax=xend_plotting_time


                if len(sample)>0 :
                    hist(sample,bins=nbins,range=(0,xmax),facecolor='green', alpha=0.75)

                plt.axis([xmin,xmax,ymin,yymax])

                if varname=='germinationtime' :
                    plt.xticks(np.arange(0,xmax*1.01,step=xticks_separation),fontsize=fz)
                    plt.yticks(fontsize=fz)
                    plt.yticks(np.arange(0,yymax*1.01,yymax/4.0), (r"0",r"25",r"50",r"75",r"100"))
                    plt.axis([xmin,xmax,ymin,yymax])

                    # print('ratio '+str(rat))

                    if (xmax-xmin) > 0 and (yymax-ymin)>0:
                    # if np.isfinite(xmax-xmin) :
                        rat = 1.0*(yymax-ymin)/(1.0*(xmax-xmin))
                        ax.set_aspect(aspect_histos/rat)
                else :
                    plt.tight_layout()

                fileout='_'+varname+'_t'+str(time)

                if varname=='germinationtime' :
                    fileout='_'+varname+'_t'+str(time)+string_single_plot+'_fintime100'
                    
                fname="histo"+fileout+".pdf"
                fullfname=dirout+'/'+fname
                plt.savefig(fullfname, format=None)
                shutil.copyfile(fullfname,dirout_histos_summary+fname)
                plt.close()


    if plot_target_vs_gaaba==1 :
        #print('plotting trajectories')
        symax=max(x['target'])*1.05;
        sxmax=max(x['ga']/x['aba'])*1.05;
        sxmax=100;
        ymin=min(x['target'])*0.5;
        xmin=min(x['ga']/x['aba'])*0.5;
        xmin=0.01;
        
        
        dirout = 'target_vs_gaaba'
        try:
            os.stat(dirout)
        except:
            os.mkdir(dirout)

        fig, (ax1) =plt.subplots(1,1)

        plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        for ii in range(0,num_germinated_seeds/2,1) :       # for loop to plot a certain number of germinated seeds
            jj=ids_germinated_seeds[ii]
            xsample=1.0*x[(x.seed_id==jj)]['ga']/x[(x.seed_id==jj)]['aba']
            ysample=x[(x.seed_id==jj)]['target']
            xsample=xsample[::1].values
            ysample=ysample[::1].values
            plt.loglog(xsample,ysample,'-',linewidth=lwthin)

    #   xsample=x[(x.time>20)]['ga']/x[(x.time>20)]['aba']
    #   ysample=x[(x.time>20)]['target']
    #   if len(xsample)>0 :
    #       plt.scatter(xsample,ysample, lw=0.8, c='r',s=100,alpha=0.7)

        plt.axhline(thresholds_for_germination,linewidth=lwth, color='green',linestyle='--',alpha=alphth)
        #plt.axvline(critical_gaaba_fraction,linewidth=lwth, color='blue',linestyle='--',alpha=alphth)

        plt.xlabel('[GA]/[ABA]')
        plt.ylabel(labels['target'])
        
        plt.axis([xmin, sxmax, ymin, symax])
        plt.tight_layout()


        fileout='_gaabaratio_vs_target'
        fname="timecourse"+fileout+".pdf"
        fullfname=dirout+'/'+fname
        plt.savefig(fullfname, format=None)
        plt.close()


    if plot_timecourse_germination_fraction==1 :
        dirout = 'timecourses'  #creating sub-directory
        try:
            os.stat(dirout)
        except:
            os.mkdir(dirout)

        fig_size=2

        fig, (ax1) =plt.subplots(1,1)                           # figures for variables
        plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

        xmin=0;#
        xmax=max(x.time)*1.01;
        xmax=20;

        ymin=0;ymax=1.05;

        xs=time_points      #x values of time course for one seed
        ys=germination_fractions      #ind is an index

        plt.plot(xs,ys,'-',linewidth=lw);

        plt.xlabel('Time (A.U.)')
        plt.ylabel('Fraction germinated seeds')
        plt.axis([xmin, xmax,ymin, ymax])
        plt.tight_layout()

        fileout='_germination_frac'
        fname="tc"+fileout+".pdf"
        fullfname=dirout+'/'+fname
        plt.savefig(fullfname, format=None)
        plt.close()

