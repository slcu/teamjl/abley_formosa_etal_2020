#!/usr/bin/env python


def gfunct(xx,basal,vmax,deg,theta,hhh) :
		# steady state of a hill decreasing regulated species
        return (1.0/deg)*(basal+1.0*vmax/(1+(xx/theta)**hhh))

def afunct(xx,basal,vmax,deg,theta,hhh) :
		# steady state of a hill increasing regulated species
        return (1.0/deg)*(basal+1.0*vmax*(xx)**hhh/((theta)**hhh+(xx)**hhh))

def zfunc1(xx,yy,basal,vmax,deg,degp,thetax,thetay,hx,hy) :
        return (basal+1.0*vmax*xx**hx/(thetax**hx+(xx)**hx))*(deg+1.0*degp*((yy)**hy/((thetay)**hy+(yy)**hy)))**(-1)