#!/usr/bin/env python

# script to find the roots of certain equations. Note some functions are defined in eqs.py

import numpy as np
import os
import json
import pandas as pd

import scipy.optimize as opt

import matplotlib
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import pylab
from pylab import *

import eqs
from eqs import *

dirini=os.getcwd()  # storing the path in which this file is located
print(dirini)

fiz=6
fz=16
lw=1.5

with open("info.json", "r") as f:
    info= json.load(f)

thresholds_for_germination=info['initial_conds_pars']['thresholds_for_germination']; # 
num_seeds=info['initial_conds_pars']['num_seeds']

deg_aba=info['model_pars']['deg_aba']; # 
deg_ga=info['model_pars']['deg_ga']; # 
basal_aba=info['model_pars']['basal_aba']; # 
basal_ga=info['model_pars']['basal_ga']; # 
bastime_ga=info['model_pars']['bastime_ga'];

basal_m=info['model_pars']['basal_m'];
deg_m=info['model_pars']['deg_m'];

vmax_aba=info['model_pars']['vmax_aba']; # 
vmax_ga=info['model_pars']['vmax_ga']; # 
seed_vol=info['model_pars']['seed_vol']; # 

theta_aba=info['model_pars']['theta_aba']; # 
theta_ga=info['model_pars']['theta_ga'] # 
hhh=info['model_pars']['hhh'] #

basal_target=info['model_pars']['basal_target']; # 
deg_target=info['model_pars']['deg_target']; # 
vmax_z=info['model_pars']['vmax_z']; #
theta_zaba=info['model_pars']['theta_zaba']; #
delta_1aba=info['model_pars']['delta_1aba']; #
h_1=info['model_pars']['h_1'] #
deg2_target=info['model_pars']['deg2_target']; #
theta_zga=info['model_pars']['theta_zga']; #
delta_2ga=info['model_pars']['delta_2ga']; #
h_2=info['model_pars']['h_2'] #

par1=info['varying_par']['par1']
var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']
dimsweep=info['varying_par']['dimsweep']


numpointsdiag=600
added_ga=0
added_aba=0

par_sweep=np.linspace(0, 10, num=numpointsdiag)

numpoints=2000
list_X=np.logspace(-9, 5, num=numpoints)


def fnullcl(x) :
	return zfunc1(afunct(x,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(x,basal_ga,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-x

nullcl=fnullcl(list_X)
bisect=np.zeros(numpoints-1)

for i in range(0,numpoints-1) :
	bisect[i]=nullcl[i]*nullcl[i+1]

indroots=np.where(bisect < 0)[0]
numroots=len(indroots)

zs=[]
for i in range(0,numroots,1) :
	y0=nullcl[indroots[i]]
	y1=nullcl[indroots[i]+1]

	x0=list_X[indroots[i]]
	x1=list_X[indroots[i]+1]

	z0 = opt.brentq(fnullcl,x0,x1)
	zs.append(z0)

zi=max(zs)
zil=min(zs)
ziuns=np.nan
if len(zs)>2 :
	ziuns=zs[1]

gi=gfunct(zi,basal_ga,vmax_ga,deg_ga,theta_ga,hhh)
ai=afunct(zi,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)

# adding these to the json dictionary
info['initial_conds_pars']['ga_ic']=gi # 
info['initial_conds_pars']['aba_ic']=ai #
info['initial_conds_pars']['target_ic']=zi; 
target_icl=zil; 

print(str(numroots)+' computed roots ! Ic is g_ic='+str(gi)+' aba_ic='+str(ai))

get_t1_fp=0
if dimsweep==2 :
	get_t1_fp=1

if get_t1_fp==1 :
	def fnullcl_t1(x) :
		return zfunc1(afunct(x,basal_aba,vmax_aba,deg_aba,theta_aba,hhh),gfunct(x,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh),basal_target,vmax_z,deg_target,deg2_target,theta_zaba,theta_zga,h_1,h_2)-x
	nullcl_t1=fnullcl_t1(list_X)
	bisect=np.zeros(numpoints-1)

	for i in range(0,numpoints-1) :
		bisect[i]=nullcl_t1[i]*nullcl_t1[i+1]

	indroots=np.where(bisect < 0)[0]
	fnumroots=len(indroots)

	zst1=[]
	for i in range(0,fnumroots,1) :
		y0=nullcl_t1[indroots[i]]
		y1=nullcl_t1[indroots[i]+1]

		x0=list_X[indroots[i]]
		x1=list_X[indroots[i]+1]

		z0 = opt.brentq(fnullcl_t1,x0,x1)
		zst1.append(z0)
	zfmax=max(zst1)
	zf=min(zst1)
	zuns=np.nan
	if len(zst1)>2 :
		zuns=zst1[1]

	gf=gfunct(zf,basal_ga+bastime_ga*basal_m/deg_m,vmax_ga,deg_ga,theta_ga,hhh)
	af=afunct(zf,basal_aba,vmax_aba,deg_aba,theta_aba,hhh)

newinfo={'analysis':{'start_num_fp' : numroots}}

if dimsweep==2 :
	newinfo={'analysis':{'start_num_fp' : numroots,'final_num_fp' : fnumroots,'target_fc': zf,'target_fc_max': zfmax,'target_funs' : zuns,'target_iuns' : ziuns,'target_icl': target_icl}}

info.update(newinfo)

with open('info.json','w') as f:
    json.dump(info, f)
