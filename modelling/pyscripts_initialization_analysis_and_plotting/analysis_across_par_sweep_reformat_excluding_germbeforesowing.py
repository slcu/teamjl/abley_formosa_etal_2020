#!/usr/bin/env python

# This python script plots organism derived results.

variables_phds=['fraction_germinated_seeds']
variables_lphds=['cv_gt','mode_gt']
variables_norm_phds=['cv_gt','mode_gt']

# Importing packages###############
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['pdf.fonttype'] = 42
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
import matplotlib.colors as mcol

import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio
import seaborn as sns

#import statistics

# extracted from vs_basal_aba_seed_vol30_deg_aba1_deg_ga1_bas_ga0.3_vmax_aba10_vmax_ga4_the_aba3.7_the_ga1.2_bas_targ0.3_deg_targ0.4_vmaxz_10_the_zaba6.5_deg2_target_6_the_zga6_basal_m_39_deg_m_0.1_bastime_ga_0.01_num_seeds4000_end_time1000_threshold2
# dose obs when bas_aba=1

#############################################
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots

# see below for fig size

dirini=os.getcwd()

with open("info.json", "r") as f:
    info= json.load(f)

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_parsexploration_path']
Results_dirout=info['strings']['Results_dirout']
num_realisations=info['initial_conds_pars']['num_realisations']
print num_realisations

#var_doses=info['strings']['var_doses']
string_sim=info['strings']['string_sim']
par1=info['varying_par']['par1']
par2=info['varying_par']['par2']
dimsweep=info['varying_par']['dimsweep']

var_par=info['strings']['var_par']
par_string=info['strings']['par_string']
label=info['strings']['label_par']

get_bifdiag=info['plotting_pars']['get_bifdiag']
end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

par1sweep_from_run_script=info['parsweep'][par1]
par2sweep_from_run_script=info['parsweep'][par2]
string_vars=info['par_string_list'][par1]+'_vs_'+info['par_string_list'][par2]


fiz=2.5
fz=12

# Plotting figures
fig_size=(fiz,fiz) 
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'
pylab.rcParams.update(params)

pylab.rcParams.update(params)

dirout = dataname_out+'/2d_plots'  #creating sub-directory
try:
    os.stat(dirout)
except:
    os.mkdir(dirout)

main_path=info['strings']['main_path']

if 'CTL_main_parsexploration_path' in info['strings'] :
    CTL_dataname_out=info['strings']['CTL_main_parsexploration_path']

filedat=dataname_out+'/dose_obs.dat' 

if dimsweep==1 :
    colnames=['val_ic',par1,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size']
elif dimsweep==2 :
    colnames=['val_ic', par1, par2,'fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size','ithtar','fthtar','start_num_fp','final_num_fp']

ymin=min(par1sweep_from_run_script)
ymax=max(par1sweep_from_run_script)
xmin=min(par2sweep_from_run_script)
xmax=max(par2sweep_from_run_script)

if 'CTL_main_parsexploration_path' in info['strings'] :
    filedat_CTL=CTL_dataname_out+'/dose_obs.dat' 
    x_CTL = pd.read_csv(filedat_CTL,sep='\t',skip_blank_lines=True,names=colnames)
    x_CTL=x_CTL.round(8) # rounding to prevent errors in matching
    x_CTL=x_CTL[(x_CTL[par2]>=xmin) & (x_CTL[par2]<=xmax) & (x_CTL[par1]>=ymin) & (x_CTL[par1]<=ymax)]
    x_CTL_excluded=x_CTL[(x_CTL['fraction_germinated_seeds'])>0]
    x_CTL_included=x_CTL[(x_CTL['fraction_germinated_seeds'])==0]


x = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=colnames)
x=x.round(8) # rounding to prevent errors in matching
x=x[(x[par2]>=xmin) & (x[par2]<=xmax) & (x[par1]>=ymin) & (x[par1]<=ymax)]


if size(x_CTL_included)>0 :
    xxx=x
    m=transpose([x_CTL_included[par1].values,x_CTL_included[par2].values]);
    points_to_keep=np.unique(m,axis=0)

    x_clean=[]
    i=0
    for i in range(0,len(points_to_keep),1) :
        xaux=x[(x[par1]==points_to_keep[i][0]) & (x[par2]==points_to_keep[i][1])]
        if len(xaux)>0 :
            if len(x_clean)==0 :
                x_clean=xaux
            else :
                x_clean=pd.concat([x_clean,xaux])
    x=x_clean 
    x=x.reset_index()

if size(x_CTL_excluded)>0 :
    m_exc=transpose([x_CTL_excluded[par1].values,x_CTL_excluded[par2].values]);
    points_to_exclude=np.unique(m_exc,axis=0)
    
#fiz=2.5
#fz=10

# Plotting figures
#fig_size=(fiz,fiz) 
#params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
#rcParams['font.family'] = 'Arial'
#pylab.rcParams.update(params)

#xforsns=x[(x.val_ic==1) & (x.ithtar==1)  (x.fthtar==1) & (x.final_num_fp<5)]
xforsns=x[(x.ithtar==1) & (x.fthtar==1) & (x.final_num_fp<5)]
xngforsns=x[(x.ithtar==1) & (x.fthtar==0) & (x.final_num_fp<5)]

region=np.NaN*np.zeros(len(x))
pdval_region=pd.DataFrame({'region':region})
              
xx= pd.concat([x,pdval_region], axis=1)

xx.loc[xx[(xx.ithtar==1) & (xx.fthtar==1) & (xx.final_num_fp==3)].index.tolist(),'region']=2
xx.loc[xx[(xx.ithtar==1) & (xx.fthtar==1) & (xx.final_num_fp==1)].index.tolist(),'region']=1
xx.loc[xx[(xx.ithtar==1) & (xx.fthtar==0) & (xx.final_num_fp==1)].index.tolist(),'region']=3

print('blue fp',list(set(xngforsns.final_num_fp.values)))

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns)
ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns[(xforsns.val_ic==1)],color='b')
ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns[(xforsns.val_ic==2)],color='orange')
ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns[(xforsns.val_ic==3)],color='g')

ax.set_yscale("log")

plt.ylabel('Mode (A.U.)',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
#plt.xticks([0,1],('Monostable \n (white region)','Bistable'),fontsize=fz-2)
plt.xticks([0,1],('Monostable','Bistable'),fontsize=fz)
ax.set(ylim=(0.1, 1000))
#plt.show()

fname='swplot_Mode_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()


fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns)
ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns[(xforsns.val_ic==1)],color='b')
ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns[(xforsns.val_ic==2)],color='orange')
ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns[(xforsns.val_ic==3)],color='g')

xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)]

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
ax.set(ylim=(0, 1))

#plt.xticks([0,1],('Monostable \n (white region)','Bistable'),fontsize=fz-2)
plt.xticks([0,1],('Monostable','Bistable'),fontsize=fz)
#plt.show()

fname='swplot_CV_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.30, bottom=0.20, right=0.85, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns)
ax=sns.swarmplot(x="final_num_fp",y="fraction_germinated_seeds",data=xforsns[(xforsns.val_ic==1)],color='b')
ax=sns.swarmplot(x="final_num_fp",y="fraction_germinated_seeds",data=xforsns[(xforsns.val_ic==2)],color='orange')
ax=sns.swarmplot(x="final_num_fp",y="fraction_germinated_seeds",data=xforsns[(xforsns.val_ic==3)],color='g')

plt.ylabel('Percentage',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
ax.set(ylim=(0, 1.05))
plt.yticks([0,0.25,0.50,0.75,1.00],('0','25','50','75','100'),fontsize=fz-2)

#plt.xticks([0,1],('Monostable \n (white region)','Bistable'),fontsize=fz-2)
plt.xticks([0,1],('Monostable','Bistable'),fontsize=fz)
#plt.show()

fname='swplot_fr_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

###
###

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="mode_gt",data=xforsns)
ax=sns.swarmplot(x="region",y="mode_gt",data=xx[(xx.val_ic==1)],color='b')
ax=sns.swarmplot(x="region",y="mode_gt",data=xx[(xx.val_ic==2)],color='orange')
ax=sns.swarmplot(x="region",y="mode_gt",data=xx[(xx.val_ic==3)],color='g')

ax.set_yscale("log")

plt.ylabel('Mode (A.U.)',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
plt.xticks([0,1,2],('Monostable G \n (white region)','Bistable','Monostable NG\n (blue region)'),fontsize=fz-2)
ax.set(ylim=(0.1, 1000))
ax.set(xlim=(-1, 3))

#plt.show()

fname='swplot_all_Mode_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()


fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns)
ax=sns.swarmplot(x="region",y="cv_gt",data=xx[(xx.val_ic==1)],color='b')
ax=sns.swarmplot(x="region",y="cv_gt",data=xx[(xx.val_ic==2)],color='orange')
ax=sns.swarmplot(x="region",y="cv_gt",data=xx[(xx.val_ic==3)],color='g')

xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)]

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(-1, 3))

plt.xticks([0,1,2],('Monostable G \n (white region)','Bistable','Monostable NG\n (blue region)'),fontsize=fz-2)
#plt.show()

fname='swplot_all_CV_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

#ax=sns.swarmplot(x="final_num_fp",y="cv_gt",data=xforsns)
ax=sns.swarmplot(x="region",y="fraction_germinated_seeds",data=xx[(xx.val_ic==1)],color='b')
ax=sns.swarmplot(x="region",y="fraction_germinated_seeds",data=xx[(xx.val_ic==2)],color='orange')
ax=sns.swarmplot(x="region",y="fraction_germinated_seeds",data=xx[(xx.val_ic==3)],color='g')

plt.ylabel('Germination Fraction',fontsize=fz)
plt.xlabel('Regime',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(-1, 3))

plt.xticks([0,1,2],('Monostable G \n (white region)','Bistable','Monostable NG\n (blue region)'),fontsize=fz-2)
#plt.show()

fname='swplot_all_fr_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()


###

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].mode_gt.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].mode_gt.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].mode_gt.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].mode_gt.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Mode (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(1, 1000))
ax.set_xscale("log")

fname='CV_vs_logMode_'+string_vars+"_bio.pdf"

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].mode_gt.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].mode_gt.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].mode_gt.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].mode_gt.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Mode (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(0, 1000))

fname='CV_vs_Mode_'+string_vars+"_bio.pdf"
# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

fig=plt.figure()
ax = fig.add_subplot(111)
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.final_num_fp==1)].mode_gt.values,xforsns[(xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.final_num_fp==3)].mode_gt.values, xforsns[(xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.final_num_fp==1)].mode_gt.values,(xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.final_num_fp==3)].mode_gt.values, (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Mode (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(0, 1000))

fname='CV_vs_Mode_'+string_vars+"_all_ics_biobio.pdf"
# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.final_num_fp==1)].mode_gt.values,xforsns[(xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.final_num_fp==3)].mode_gt.values, xforsns[(xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.final_num_fp==1)].mode_gt.values,(xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.final_num_fp==3)].mode_gt.values, (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Mode (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1))
ax.set(xlim=(0, 200))

fname='CV_vs_Mode_'+string_vars+"_tmax200_all_ics_biobio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()


fig=plt.figure()
ax = fig.add_subplot(111) 
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].fraction_germinated_seeds.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].fraction_germinated_seeds.values,xforsns[(xforsns.val_ic==1) & (xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].fraction_germinated_seeds.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].fraction_germinated_seeds.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Fraction (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1.0))
ax.set(xlim=(0, 1.03))
#ax.set_xscale("log")

fname='CV_vs_fracseeds_'+string_vars+"_bio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

fig=plt.figure()
ax = fig.add_subplot(111)
plt.subplots_adjust(left=0.20, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

plt.scatter(xforsns[(xforsns.final_num_fp==1)].fraction_germinated_seeds.values,xforsns[(xforsns.final_num_fp==1)].cv_gt.values,color='w',edgecolors='k',alpha=0.5)
plt.scatter(xforsns[(xforsns.final_num_fp==3)].fraction_germinated_seeds.values,xforsns[(xforsns.final_num_fp==3)].cv_gt.values,color='k',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].fraction_germinated_seeds.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==1)].cv_gt.values,color='b',alpha=0.5)
#plt.scatter(xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].fraction_germinated_seeds.values,xngforsns[(xngforsns.val_ic==1) & (xngforsns.final_num_fp==3)].cv_gt.values,color='m',alpha=0.5)

plt.ylabel('CV',fontsize=fz)
plt.xlabel('Fraction (A.U.)',fontsize=fz)
ax.set(ylim=(0, 1.0))
ax.set(xlim=(0, 1.03))
#ax.set_xscale("log")

fname='CV_vs_fracseeds_'+string_vars+"_all_ics_biobio.pdf"

# saving the figure
plt.savefig(dirout+'/'+fname)
shutil.copyfile(dirout+'/'+fname,Results_dirout+'/sims_summaries/'+fname)
plt.close()

stts=[]
stts_ics=[]

for jj in range(1,4,1) :
    xmdcv=[]
    xmncv=[]
    xcvcv=[]
    xmdm=[]
    xmnm=[]
    xcvm=[]
    for ii in range(1,len(set(xx.val_ic.values))+1) :
        zz=xx[(xx.val_ic==ii) & (xx.region==jj)].mode_gt
        zz = zz[~np.isnan(zz)]
        
        mdm=np.median(zz)
        mnm=np.mean(zz)
        stdm=np.std(zz)
        
        cvm=stdm/mnm
        xmdm.append(mdm)
        xmnm.append(mnm)
        xcvm.append(cvm)

        yy=xx[(xx.val_ic==ii) & (xx.region==jj)].cv_gt
        yy = yy[~np.isnan(yy)]
        
        mdcv=np.median(yy)
        mncv=np.mean(yy)
        stdcv=np.std(yy)
        
        cvcv=stdcv/mncv
        xmdcv.append(mdcv)
        xmncv.append(mncv)
        xcvcv.append(cvcv)

        stts_ics.append([ii,mdm,mnm,cvm,mdcv,mncv,cvcv,jj])

    mdm_ics=np.mean(xmdm)
    mnm_ics=np.mean(xmnm)
    cvm_ics=np.mean(xcvm)
    
    mdcv_ics=np.mean(xmdcv)
    mncv_ics=np.mean(xmncv)
    cvcv_ics=np.mean(xcvcv)
    
    stts.append([mdm_ics,mnm_ics,cvm_ics,mdcv_ics,mncv_ics,cvcv_ics,jj])

x_stts=pd.DataFrame(stts,columns=['mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region'])
x_stts.to_csv(path_or_buf=dataname_out+'/'+'stats.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

print(dataname_out+'/'+'stats.dat')

x_stts_ics=pd.DataFrame(stts_ics,columns=['val_ic','mdMode','mnMode','cvMode','mdCV','mnCV','cvCV','region'])
x_stts_ics.to_csv(path_or_buf=dataname_out+'/'+'stats_ics.dat', sep='\t',  header=None, index=None, index_label=None, line_terminator='\n', date_format=None, decimal='.')

