#!/usr/bin/env python

# This python script plots organism derived results.

variables_to_plot=['fraction_germinated_seeds','mean_gt','q2_gt','cv_gt','size','mode_gt','cvq_gt']

# Importing packages###############
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.ticker as ScalarFormatter
import matplotlib.axes as ax
matplotlib.rcParams['pdf.fonttype'] = 42

import pylab
from pylab import *
import subprocess
from subprocess import call

import os
import sys
import subprocess
import shutil
import pandas as pd
import pickle
import json
import scipy
from scipy import stats
import scipy.io as sio
#import statistics

#############################################
fiz=1.77  # Figure size
lw=2.0 # linewidth of the plots
fz=12  # font size for the different plots


dirini=os.getcwd()

with open("info.json", "r") as f:
    info= json.load(f)

num_seeds=info['initial_conds_pars']['num_seeds']

dataname_out=info['strings']['main_doses_path']
num_realisations=info['initial_conds_pars']['num_realisations']
var_doses=info['strings']['var_doses']
dose_string=info['strings']['dose_string']
string_sim=info['strings']['string_sim']

end_plotting_time=info['plotting_pars']['end_plotting_time']*1.05
ticks_separation=info['plotting_pars']['ticks_separation']

filedat=dataname_out+'/dose_obs_layout_for_plotting.dat' 

x = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=['val_ic','added_aba','added_ga','fraction_germinated_seeds','cv_gt','mean_gt','std_gt','mode_gt','min_gt','max_gt','range_gt','q1_gt','q2_gt','q3_gt','cvq_gt','size'])

ics=x['val_ic'].unique()


# Labels for paper
labels={'fraction_germinated_seeds':'Percentage','mean_gt':'Mean (A.U.)','added_ga':'Exogenous GA (A.U.)','added_aba':'Exogenous ABA (A.U.)','cv_gt':'CV','q2_gt':'Median (A.U.)','cvq_gt': 'IQR/median ','mode_gt':'Mode (A.U.)','size':'Germinated seeds'}
filelabels={'fraction_germinated_seeds':'fr_germ_seeds','mean_gt':'mean_gt','q2_gt':'median_gt','cv_gt':'cv_gt','cvq_gt':'cvq_gt','mode_gt':'mode_gt','size':'gseedsnum'}


filedat=dataname_out+'/bif.dat' 
x_bif = pd.read_csv(filedat,sep='\t',skip_blank_lines=True,names=['added_aba','added_ga','numroots','branch','z0','g0','a0','bif','cumulativebif','underthreshold'])

#print(x_bif)
bifurcation_pts=x_bif[(x_bif['bif']==1)&(x_bif['branch']==0)]
auxthresholdcross_pt=x_bif[(x_bif['underthreshold']==0)&(x_bif['branch']==0)&(x_bif['cumulativebif']==0)]

plot_threshold_bif=0
if size(auxthresholdcross_pt)>0 :
    plot_threshold_bif=1
    thresholdcross_pt=auxthresholdcross_pt.iloc[0]


# Plotting figures
fig_size=(fiz,fiz)   #for starting the plots
params ={'axes.labelsize': fz,'font.size': fz,'xtick.labelsize': fz,'ytick.labelsize': fz,'figure.figsize': fig_size,'mathtext.default': 'regular'}
rcParams['font.family'] = 'Arial'

pylab.rcParams.update(params)


dirout = dataname_out+'/dose_responses_plots'  #creating sub-directory
try:
    os.stat(dirout)
except:
    os.mkdir(dirout)

fig_size=2

for ii in range(0,len(variables_to_plot),1) :                   #for loop for specific variables
    fig, (ax1) =plt.subplots(1,1)                           # figures for variables
    plt.subplots_adjust(left=0.30, bottom=0.20, right=None, top=None, wspace=0.5, hspace=0.6)

    var=variables_to_plot[ii]    #get name of variable 
    xmin=min(x[var_doses].values)*0.2;
    xmax=max(x[var_doses].values)*1.1;
    xxmax=max(x[var_doses].values);

    ymin=0;#ymax=max(x[var])*1.15;
    xmin=0.0;
    xmin=0.007;
    xmax=15.01
    
    #scale='log'
    scale='linear'
    for jj in range(0,num_realisations,1) :        #for loop to get certain seeds to plot
        ind=ics[jj]                   #seed_indices defined at start
        xs=x[(x.val_ic==ind)][var_doses].values      #x values of time course for one seed
        ys=x[(x.val_ic==ind)][var].values      #ind is an index
 # note we suppress the error bars for the article, putting x in the next conditional
        if var=='xmean_gt' :
            zs=x[(x.val_ic==ind)]['std_gt'].values
            plt.errorbar(xs, ys, yerr=zs,fmt='-o',linewidth=lw,alpha=0.8)
            ymax=(max(x[var])+max(zs))*1.2
  # note we suppress the error bars for the article, putting x in the next conditional
        elif var=='xq2_gt' :
            z1=x[(x.val_ic==ind)]['q1_gt'].values
            z2=x[(x.val_ic==ind)]['q2_gt'].values
            z3=x[(x.val_ic==ind)]['q3_gt'].values
            ymax=(max(x[var])+(max(x['q3_gt'])-max(x[var])))*1.05
            plt.errorbar(xs, ys, yerr=[(z2-z1),(z3-z2)],fmt='-o',linewidth=lw,alpha=0.8)                        
        else :
            plt.plot(xs,ys,'-o',linewidth=lw);
    plt.tick_params(axis='x', which='major')

    plt.xscale(scale, nonposy='clip');

    plt.xticks(np.linspace(0,10,num=11), (r"0",r"1",r"2",r"3",r"4",r"5",r"6",r"7",r"8",r"9",r"10",r"11"),fontsize=fz)

    if xxmax>5 :
        plt.xticks(np.linspace(0,10,num=6), (r"0",r"2",r"4",r"6",r"8",r"10"),fontsize=fz)

    plt.yticks(np.arange(0,end_plotting_time,step=ticks_separation),fontsize=fz)
    

    if var=='size' :
        ymax=num_seeds*1.2


    if var=='cv_gt' :
        ymax=0.82
        plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)

    if var=='cvq_gt' :
        ymax=max(ymax,0.82)
        plt.yticks(np.arange(0,ymax*1.1,step=0.2),fontsize=fz)

    if var=='fraction_germinated_seeds' :
        ymax=max(x[var])*1.1
        #plt.yticks(np.arange(0,ymax,step=0.1),fontsize=fz)
        plt.yticks(np.arange(0,1.01,step=0.25),(r"0",r"25",r"50",r"75",r"100"),fontsize=fz)

    if var=='mean_gt' :
        ymax=end_plotting_time*0.95

    if var=='mode_gt' :
        ymax=end_plotting_time*0.95


    if var=='q2_gt' :
        ymax=end_plotting_time*0.95


    if plot_threshold_bif==1 :
        xo=thresholdcross_pt[var_doses]
        plt.axvline(x=xo,alpha=0.5)
    
    plt.xscale('log');
    if var=='size' :
        plt.yscale('log');
        ymin=5;
    plt.subplots_adjust(bottom=0.5)

    if not(isnan(ymax)) :
        plt.axis([xmin,xmax,ymin,ymax])
    
    rat = (ymax-ymin)/(xmax-xmin)

    ax1.set_aspect(1.0/rat)
    plt.xticks([0.01,0.1,1,10], (0,0.1,1,10))

    plt.tight_layout()

    fileout='_'+var
    fname=string_sim+dose_string+"_dose"+fileout+".pdf"    
    fullfname=dirout+'/'+fname
    plt.savefig(fullfname, format=None)
    plt.close()
